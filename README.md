## The official Page for contributing to this project.

- This project is divided into branches for every component of the MVC design pattern, with that said the three main branches will be as follows:

|\
| Master: the default branches that will be used to merge the code of the different branches
|/
|\
| Model: this branch will handle the code related to database and models
|/
|\
| View: this branch will handle the code related to views
|/
|\
| Controller: this branch will handle the code related to controllers
|/

<b>Note: we will keep adding new branches and updating the default ones as the development process move on</b>