@extends('errors::minimal')
@section('title', __('خارج الخدمة'))
@section('code', 'يرجى المحاولة لاحقاً')
@section('message', __('التطبيق قيد الصيانة'))
