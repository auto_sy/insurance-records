@extends('templates.show')

@section('title')
    معلومات الدفعة النقدية | الحوالة
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active"><i class="fa fa-id-card-o"></i> عرض الدفعة النقدية | الحوالة البدائية <i
            class="fa fa-address-card" aria-hidden="true"></i></li>
    <li class="breadcrumb-item active"><a href="{{ route('payment.list') }}">الدفعات النقدية | الحوالات البدائية <i class="fa fa-table"
                aria-hidden="true"></i></a></li>
    <li class="breadcrumb-item active"><a href="{{route('welcome')}}">الرئيسية <i class="fa fa-tachometer-alt"></i></a></li>
@endsection

@section('card-title')
    معلومات الدفعة النقدية | الحوالة البدائية
@endsection

@section('table')
    <table class="table table-bordered table-striped table-hover table-sm"
        style="table-layout: fixed;text-align:center;align-items:center;justify-content:center;">
        <tr class="d-flex">
            <th class="col-md-2">اسم العارض</th>
            <td class="col-md-4">{{ $payment->bidder_name }}</td>
            <th class="col-md-2">القيمة</th>
            <td class="col-md-4">{{ $payment->value }}</td>
        </tr>
        <tr class="d-flex">
            <th class="col-md-2">العملة</th>
            <td class="col-md-4">{{ $payment->currency }}</td>
            <th class="col-md-2">المكافئ بالليرة السورية</th>
            <td class="col-md-4">{{ $payment->equ_val_sy ?? 'لايوجد' }}</td>
        </tr>
        <tr class="d-flex">
            <th class="col-md-2">رقم الدفعة أو الحوالة</th>
            <td class="col-md-4">{{ $payment->number }}</td>
            <th class="col-md-2">الحالة</th>
            <td class="col-md-4">{{ $payment->status }}</td>
        </tr>
        <tr class="d-flex">
            <th class="col-md-2">تاريخ التقديم</th>
            <td class="col-md-4">{{ $payment->date }}</td>
            <th class="col-md-2">النوع</th>
            <td class="col-md-4">{{ $payment->type }}</td>
        </tr>
        <tr class="d-flex">
            <th class="col-md-2">اسم المصرف</th>
            <td class="col-md-4">{{ $bank_name ?? 'لايوجد' }}</td>
        </tr>
        <tr class="d-flex">
            <th class="col-md-2">الموضوع</th>
            <td class="col-md-10">{{ $payment->matter }}</td>
        </tr>
        <tr class="d-flex">
            <th class="col-md-2">ملاحظات</th>
            <td class="col-md-10">{{ $payment->notes }}</td>
        </tr>
    </table>
@endsection

@section('card-footer')
    @if (Auth::user()->hasPermission('initial_records-input'))
        <div class="col-xs-1">
            <form action="{{ route('payment.releaseForm', ['id' => $payment->id]) }}" class="form-inline">
                <button type="Submit" class="btn btn-outline-primary">تحرير</button>
            </form>
        </div>
        <div class="col-xs-1">
            <form action="{{ route('payment.requiseForm', ['id' => $payment->id]) }}" class="form-inline">
                <button type="Submit" class="btn btn-outline-primary">مصادرة</button>
            </form>
        </div>
    @else
        <div class="col-xs-1">
            <form class="form-inline">
                <button type="Submit" class="btn btn-outline-primary" disabled>تحرير</button>
            </form>
        </div>
        <div class="col-xs-1">
            <form class="form-inline">
                <button type="Submit" class="btn btn-outline-primary" disabled>مصادرة</button>
            </form>
        </div>
    @endif
    <div class="col-xs-1">
        <form action="{{ route('payment.edit', ['id' => $payment->id]) }}" class="form-inline">
            @if (Auth::user()->hasPermission('initial_records-edit'))
                <button type="Submit" class="btn btn-outline-primary">تعديل</button>
            @else
                <button type="Submit" class="btn btn-outline-primary" disabled>تعديل</button>
            @endif
        </form>
    </div>
@endsection
